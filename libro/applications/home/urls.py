from django.urls import path, re_path

from . import views

app_name="home_app"

"""
name = Index, es una etuqueta por que la url puede ser muy larga
"""
urlpatterns = [
    path('index', views.IndexView.as_view(), name="index"),
    path('libros', views.ListaLibros.as_view(), name="lista"),

]
