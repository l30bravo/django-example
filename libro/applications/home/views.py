from django.shortcuts import render

from django.views.generic import (
    TemplateView,
    ListView,
)
# Create your views here.

"""
lo que va entre parentesis en una clases un la forma de representar una herencia
TemplateView es una vista generica.
"""
class IndexView(TemplateView):
    #print("=============== Prueba de URL =======================")
    # que una vista procesa los datos y renderiza los datos en pantalla
    # siempre va a pedir un template para trabajarself.
    # un template simpre va a ser una archivo HTML.
    template_name = "home/index.html"

class ListaLibros(ListView):
    template_name = "home/lista.html"
    # esto es una lista en python
    queryset = ['El Quijote de la mancha', 'Codigo Limpio', 'La Sombra del Viente', 'Django 2.0']
    context_objet_name = 'libros'
