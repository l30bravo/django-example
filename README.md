# Comandos

## Ejecutar ambiente virtual de trabajo
```bash
 mkvirtualenv libro
```

```bash
workon libro
```

## Instalacion Django Stable

```bash
pip install Django==2.1.4
```

## Crear proyecto Django
```bash
 django-admin startproject libro
```

```bash
cd libro
```

## Ejecar servidor
```bash
 python manage.py runserver
```

## Crear una aplicacion 
```bash
mkdir applications
```

```bash
django-admin startapp  $nombre
```

